﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverProblem
{
    public class MiniHelicopter : IPosition
    {
        public int miniHX { get; set; }
        public int miniHY { get; set; }
        public MiniHelicopter() {

            miniHX = miniHY = 0;
          //Y 
        }
        public double AreaCalculation(List<int> Area)
        {
            int area = 0;
            double percentageArea = 0;

            //area surrounded by 3x3 cells
            if ((miniHX + 1 < Area[0])&& (miniHX - 1> 0)&& (miniHY + 1 < Area[1])&& (miniHY - 1 > 0))
            {
                area = 9;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);

            }
            //lower right corner
            else if ((miniHX + 1 > Area[0]) && (miniHX - 1 >= 0) && (miniHY + 1 >Area[1]) &&(miniHY -1 >= 0)) {
                area = 4;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);
            }
            //upper right corner
            else if ((miniHX + 1 > Area[0])  && (miniHX - 1 >=0) && (miniHY + 1 <Area[1]) && (miniHY - 1 <= 0))
            {
                area = 4;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);
            }
            //Upper left corner
            else if ((miniHX -1 <= 0)  && (miniHX + 1 <Area[0]) && (miniHY + 1 <Area[1]) && (miniHY - 1 <= 0))
            {
                area = 4;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);
            }

            //Lower left corner
            else if ((miniHX - 1 <= 0) && (miniHX + 1 < Area[0]) && (miniHY + 1 > Area[1]) && (miniHY - 1 >= 0))
            {
                area = 4;
                percentageArea = ((double)area * 100 )/ ((double)Area[0] * (double)Area[1]) ;
            }
            //top middle  area
            else if ((miniHX - 1 >0) && (miniHX + 1 < Area[0])&&(miniHY + 1 < Area[1])  && (miniHY - 1 <= 0))
            {
                area = 6;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);
            }
            //right middle area
            else if ((miniHX + 1 > Area[0]) && (miniHX - 1 >= 0) && (miniHY + 1 < Area[1]) && (miniHY - 1 >= 0))
            {
                area = 6;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);
            }
            //bottom middle area
            else if ((miniHX + 1 <Area[0]) && (miniHX - 1 >=0) && (miniHY + 1 > Area[1])  && (miniHY - 1 >= 0))
            {
                area = 6;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);
            }
            //left middle area
            else if ((miniHX + 1 < Area[0]) && (miniHX - 1 <= 0) && (miniHY + 1 < Area[1])  && (miniHY - 1 >= 0))
            {
                area = 6;
                percentageArea = ((double)area * 100) / ((double)Area[0] * (double)Area[1]);
            }
            return percentageArea;
        }

        public int StartMoving(List<int> Area, string Command)
        {
            throw new NotImplementedException();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverProblem
{
    public interface IPosition
    {
        int StartMoving(List<int> Area, string Command);
        double AreaCalculation(List<int> Area);

    }
}

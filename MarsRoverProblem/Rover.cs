﻿using System;
using System.Collections.Generic;

namespace MarsRoverProblem
{
    public enum Directions
    {
        N = 1,//North
        S = 2,//South
        E = 3,//East
        W = 4//West
    }
    public enum XaxisLetters
    { 
        a=1,
        b=2,
        c=3,
        d=4,
        e=5,
        f=6,
        g=7,
        h=8,
        i=9,
        j=10,
        k=11,
        l=12,
        m=13,
        n=14,
        o=15,
        p=16,
        q=17,
        r=18,
        s=19,
        t=10,
        u=21,
        v=22,
        w=23,
        x=24,
        y=25
    }

    public enum CommandList
    {
        F,
        L,
        R,
        B,
        H
    }
    public class Rover : IPosition
    {
       public int X { get; set; }
       public int Y { get; set; }
       public double InvestigatedArea { get; set; }
       public Directions Direction { get; set; }
        public Rover()
        { 
            Random random = new Random();
            //X = 8;
            //Y = 17;
            X = Convert.ToInt32(random.Next(0, 25));
            Y = Convert.ToInt32(random.Next(0, 25));
            Direction = Directions.N;
        }

        public string GetAlphabetForXaxis(int pos) {
            string XaxisLetter = string.Empty;
            foreach (int i in Enum.GetValues(typeof(XaxisLetters)))
            {
                if (pos == i)
                {
                    XaxisLetter = Enum.GetName(typeof(XaxisLetters), i);
                    break;
                }
            }
            return XaxisLetter;
        }

        public int CommandChecker(string Commands)
        {
            foreach (var command in Commands)
            {
              
               if (!(command.ToString()=="F"|| command.ToString()=="B"|| command.ToString()=="R"|| command.ToString()=="L" || command.ToString()=="H"))
                {
                    return -1;
                   // break;
                }   
            }
            return 1;
        }
        private void TurnLeft()
        {
            switch (this.Direction)
            {
                case Directions.N:
                    this.Direction = Directions.W;
                    break;
                case Directions.S:
                    this.Direction = Directions.E;
                    break;
                case Directions.E:
                    this.Direction = Directions.N;
                    break;
                case Directions.W:
                    this.Direction = Directions.S;
                    break;
                default:
                    break;
            }

        }

        private void TurnRight()
        {
            switch (this.Direction)
            {
                case Directions.N:
                    this.Direction = Directions.E;
                    break;
                case Directions.S:
                    this.Direction = Directions.W;
                    break;
                case Directions.E:
                    this.Direction = Directions.S;
                    break;
                case Directions.W:
                    this.Direction = Directions.N;
                    break;
                default:
                    break;
            }
            
        }

        private void MoveForward()
        {
            switch (this.Direction)
            {
                case Directions.N:
                    this.Y -= 1;
                    break;
                case Directions.S:
                    this.Y += 1;
                    break;
                case Directions.E:
                    this.X += 1;
                    break;
                case Directions.W:
                    this.X -= 1;
                    break;
                default:
                    break;
            }

        }

        private void MoveBackWord()
        {
            switch (this.Direction)
            {
                case Directions.N:
                    this.Y += 1;
                    break;
                case Directions.S:
                    this.Y -= 1;
                    break;
                case Directions.E:
                    this.X -= 1;
                    break;
                case Directions.W:
                    this.X += 1;
                    break;
                default:
                    break;
            }
        }
        private void LauchMiniHeliCopter(int X, int Y, List<int> Area)
        {

            MiniHelicopter miniHelicopter = new MiniHelicopter() {
                miniHX = X,
                miniHY = Y
            };
            InvestigatedArea = miniHelicopter.AreaCalculation(Area);
            Direction = Directions.N;
        }
        public int StartMoving(List<int> Area, string Commands)
        {
            if (CommandChecker(Commands)==1)
            {
                
                foreach (var command in Commands)
                {

                    switch (command)
                    {
                        case 'F':
                            this.MoveForward();
                            break;
                        case 'B':
                            this.MoveBackWord();
                            break;
                        case 'L':
                            this.TurnLeft();
                            break;
                        case 'R':
                            this.TurnRight();
                            break;
                        case 'H':
                            this.LauchMiniHeliCopter(this.X, this.Y, Area);
                            break;
                        default:
                            Console.WriteLine($"Wrong command {command}");
                            break;
                    }

                    if (this.X < 1 || this.X > Area[0] || this.Y < 1 || this.Y > Area[1])
                    {

                        if (this.X < 1)
                            this.X = 1;
                        else if (this.Y < 1)
                            this.Y = 1;
                        else if (this.X < 0 && this.Y < 0)
                        {
                            this.X = 1;
                            this.Y = 1;
                        }
                        else if (this.X > 25)
                            this.X = 25;
                        else if (this.Y > 25)
                            this.Y = 25;
                        else if (this.X > 25 && this.Y > 25)
                        {
                            this.X = 25;
                            this.Y = 25;
                        }
                        string xLetter = GetAlphabetForXaxis(this.X);
                        break;
                        //throw new Exception($"Rover can not move out of area (1 , 1) and ({Area[0]} , {Area[1]}). Roller is stopped at last point {xLetter}{this.Y}");
                    }
                }
                return 1;
            }
            else
                return -1;
        }
        
         public double AreaCalculation(List<int> Area)        {
            throw new NotImplementedException();
        }
    }
}

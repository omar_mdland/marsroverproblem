﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRoverProblem
{
    public class Program
    {
        static void Main(string[] args)
        {
            int maxX = 25;
            int maxY = 25;
            List<int> Area = new List<int>();
            Area.Add(maxX);
            Area.Add(maxY);
            Rover roverPosition = new Rover();
            string LandingPositionX = string.Empty;
            LandingPositionX = roverPosition.GetAlphabetForXaxis(roverPosition.X);
            Console.WriteLine($"Landing Position : {LandingPositionX}{roverPosition.Y}");
            Console.WriteLine("Enter Command: ");
            var Commands = Console.ReadLine().ToUpper();
            
            try
            {
                int result=roverPosition.StartMoving(Area, Commands);
                if (result != -1)
                {
                    string xLetter = string.Empty;
                    xLetter = roverPosition.GetAlphabetForXaxis(roverPosition.X);
                    Console.WriteLine($"The current position of the Rover is at: {xLetter}{roverPosition.Y}");
                    if (Commands.Contains("H"))
                       Console.WriteLine($"Investigated Area: {roverPosition.InvestigatedArea}%");
                }
                else
                {
                    Console.WriteLine("Wrong Command");
                    string xLetter = string.Empty;
                    xLetter = roverPosition.GetAlphabetForXaxis(roverPosition.X);
                    Console.WriteLine($"The Rover is in same position: {xLetter}{roverPosition.Y}");
                    if(Commands.Contains("H"))
                    Console.WriteLine($"Investigated Area: {roverPosition.InvestigatedArea}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}

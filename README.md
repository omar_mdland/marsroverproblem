# Mars Rover Problem Solution in C#

How to run:

This solution is built using C# in Visual Studio. To run this project you need Visual Studio.

1. Clone/Download the source code from bit bucket:

   git clone https://omar_mdland@bitbucket.org/omar_mdland/marsroverproblem.git

2. Open the MarsRoverProblem folder. 

3. Two ways to run and test the project. 

   - Open the MarsRoverProblem.sln file in visual studio and click build project OR
   - Open the MarsRoverProblem. Go to below directory bin->Debug. Run the MarsRoverProblem.exe

4. Unit test also integrated with it. In Visual studio  Go to Test Menu->Run All Tests. It will show the Test Explorer. You can runt the single or each test cases.

5. The coordinates starts from (1,1) and area is (1, 1) to (25,25) 

# Input:

The first line will display the initial landing position of the Mars Rover. This one is randomly selected. It will show the X axis alphabet with Y axis coordinate. Example:  h17 means ( X = 8 and Y= 17). Instead of displaying 8 for X it will show the alphabet of the position.

The second line is a series of instructions telling the rover how to explore the selected area. This instructions are given by the user. The instruction set is:

·    F: move one step forward

·    B: move backward one step

·    L: turn left (90 Degree)

·    R: turn right (90 Degree)

·    H: Launch Ingenuity (When Mini-helicopter Ingenuity is flying, Percy can not move.)

It will not process any command which have any other letter except these letters. Initially after the landing the Rover will be North Facing or when it stopped to launch Helicopter it would be North Facing.

The directions are  N, S, E, W.

Each rover will be finished the instruction sequentially and will Return the Final location as like above (h17). 

If the instruction leads the rover out of area it would display the last coordinates where it is stopped.

# Output:

The output for each rover should be its final co-ordinates and heading.

Test Input:

FFFLFFRBBHFFFFF

Expected Output:

**f11** (if the Landing position : h17) or others, depends on landing position
﻿using System.Collections.Generic;
using MarsRoverProblem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRoverProblemTest
{
   
    [TestClass]
    public class MarsRoverProblemTest
    {

        [TestMethod]
        public void TestScanrio_a2N_LFLFLFLFF()
        {
            Rover position = new Rover()
            {
                X = 1,
                Y = 2,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "LFLFLFLFF";
            position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var expectedOutput = "a2";

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [TestMethod]
        public void TestScanrio_c3N_FRRFFRFRRF()
        {
            Rover position = new Rover()
            {
                X = 3,
                Y = 3,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "FRRFFRFRRF";

            position.StartMoving(Area, Command);
            string xLetter = string.Empty;
            xLetter = XaxisLetter(position.X);
            var actualOutput = $"{xLetter}{position.Y}";
            var expectedOutput = "c4";

            Assert.AreEqual(expectedOutput, actualOutput);
        }
        [TestMethod]
        public void TestScanrio_h17N_FFFLFFRBBHFFFFF_without_AREA()
        {
            Rover position = new Rover()
            {
                X = 8,
                Y = 17,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "FFFLFFRBBHFFFFF";
            position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var expectedOutput = "f11";
            Assert.AreEqual(expectedOutput, actualOutput);
           
        }

        [TestMethod]
        public void TestScanrio_h17N_FFFLFFRBBHFFFFF_with_AREA()
        {
            Rover position = new Rover()
            {
                X = 8,
                Y = 17,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "FFFLFFRBBHFFFFF";
            position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var expectedOutput = "f11";
            var areaActual = position.InvestigatedArea;
            double expectedAreaOutput = 1.44;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }


        [TestMethod]
        public void TestScanrio_h17N_WASDRRRLLLSDD()
        {
            Rover position = new Rover()
            {
                X = 8,
                Y = 17,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "WASDRRRLLLSDD";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            //var actualOutput = $"{Xaxis}{position.Y}";
            var actualOutput = result;
            var expectedOutput = -1;

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [TestMethod]
        public void TestScanrio_w23N_RFFF()
        {
            Rover position = new Rover()
            {
                X = 23,
                Y = 23,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "RFFF";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var expectedOutput = "y23";

            Assert.AreEqual(expectedOutput, actualOutput);
        }


        [TestMethod]
        public void TestScanrio_w23N_RFRFF()
        {
            Rover position = new Rover()
            {
                X = 23,
                Y = 23,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "RFRFF";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var expectedOutput = "x25";

            Assert.AreEqual(expectedOutput, actualOutput);
        }
        [TestMethod]
        public void TestScanrio_q4N_LFRFFFF()
        {
            Rover position = new Rover()
            {
                X = 17,
                Y = 4,
                Direction = Directions.N
            };
            var Area = new List<int>() { 25, 25 };
            var Command = "LFRFFFF";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var expectedOutput = "p1";

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [TestMethod]
        public void TestScanrio_a2N_FH_With_AREA()
        {
            Rover position = new Rover()
            {
                X = 1,
                Y = 2,
                Direction = Directions.N
            };
           
            var Area = new List<int>() { 25, 25 };
            var Command = "FH";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var areaActual = position.InvestigatedArea;
            var expectedOutput = "a1";
            double expectedAreaOutput = 0.64;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }

        [TestMethod]
        public void TestScanrio_y2N_FH_With_AREA()
        {
            Rover position = new Rover()
            {
                X = 25,
                Y = 2,
                Direction = Directions.N
            };

            var Area = new List<int>() { 25, 25 };
            var Command = "FH";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var areaActual = position.InvestigatedArea;
            var expectedOutput = "y1";
            double expectedAreaOutput = 0.64;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }

        [TestMethod]
        public void TestScanrio_a24N_LLFH_With_AREA()
        {
            Rover position = new Rover()
            {
                X = 1,
                Y = 24,
                Direction = Directions.N
            };

            var Area = new List<int>() { 25, 25 };
            var Command = "LLFH";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var areaActual = position.InvestigatedArea;
            var expectedOutput = "a25";
            double expectedAreaOutput = 0.64;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }


        [TestMethod]
        public void TestScanrio_l24N_LLFH_With_AREA()
        {
            Rover position = new Rover()
            {
                X = 12,
                Y = 24,
                Direction = Directions.N
            };

            var Area = new List<int>() { 25, 25 };
            var Command = "LLFH";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var areaActual = position.InvestigatedArea;
            var expectedOutput = "l25";
            double expectedAreaOutput = 0.96;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }


        [TestMethod]
        public void TestScanrio_y24N_LLFH_WITH_AREA()
        {
            Rover position = new Rover()
            {
                X = 25,
                Y = 24,
                Direction = Directions.N
            };

            var Area = new List<int>() { 25, 25 };
            var Command = "LLFH";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var areaActual = position.InvestigatedArea;
            var expectedOutput = "y25";
            double expectedAreaOutput = 0.64;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }

        [TestMethod]
        public void TestScanrio_b14N_LFH_WITH_AREA()
        {
            Rover position = new Rover()
            {
                X = 2,
                Y = 14,
                Direction = Directions.N
            };

            var Area = new List<int>() { 25, 25 };
            var Command = "LFH";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var areaActual = position.InvestigatedArea;
            var expectedOutput = "a14";
            double expectedAreaOutput = 0.96;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }

        [TestMethod]
        public void TestScanrio_x14N_RRFH_WITH_AREA()
        {
            Rover position = new Rover()
            {
                X = 24,
                Y = 14,
                Direction = Directions.N
            };

            var Area = new List<int>() { 25, 25 };
            var Command = "RFH";
            int result = position.StartMoving(Area, Command);
            string Xaxis = string.Empty;
            Xaxis = XaxisLetter(position.X);
            var actualOutput = $"{Xaxis}{position.Y}";
            var areaActual = position.InvestigatedArea;
            var expectedOutput = "y14";
            double expectedAreaOutput = 0.96;

            Assert.AreEqual(expectedOutput, actualOutput);
            Assert.AreEqual(expectedAreaOutput, areaActual);
        }
        public static string XaxisLetter(int xAxis) {

            string XaxisLetter = string.Empty;
            switch (xAxis)
            {
                case 1:
                    XaxisLetter = "a";
                    break;
                case 2:
                    XaxisLetter = "b";
                    break;
                case 3:
                    XaxisLetter = "c";
                    break;
                case 4:
                    XaxisLetter = "d";
                    break;
                case 5:
                    XaxisLetter = "e";
                    break;
                case 6:
                    XaxisLetter = "f";
                    break;
                case 7:
                    XaxisLetter = "g";
                    break;
                case 8:
                    XaxisLetter = "h";
                    break;
                case 9:
                    XaxisLetter = "i";
                    break;
                case 10:
                    XaxisLetter = "j";
                    break;
                case 11:
                    XaxisLetter = "k";
                    break;
                case 12:
                    XaxisLetter = "l";
                    break;
                case 13:
                    XaxisLetter = "m";
                    break;
                case 14:
                    XaxisLetter = "n";
                    break;
                case 15:
                    XaxisLetter = "o";
                    break;
                case 16:
                    XaxisLetter = "p";
                    break;
                case 17:
                    XaxisLetter = "q";
                    break;
                case 18:
                    XaxisLetter = "r";
                    break;
                case 19:
                    XaxisLetter = "s";
                    break;
                case 20:
                    XaxisLetter = "t";
                    break;
                case 21:
                    XaxisLetter = "u";
                    break;
                case 22:
                    XaxisLetter = "v";
                    break;
                case 23:
                    XaxisLetter = "w";
                    break;
                case 24:
                    XaxisLetter = "x";
                    break;
                case 25:
                    XaxisLetter = "y";
                    break;
                default:
                    break;
            }
            return XaxisLetter;
        }
    }
}
